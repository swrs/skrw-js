# skrw-js

*skrw-js* is a package of JS and styles for the *Ruby on Rails* engine [*skrw*](https://gitlab.com/swrs/skrw).

## Installation

Add *skrw-js* to dependencies:

```bash
$ yarn add https://gitlab.com/swrs/skrw-js.git
```

*NOTE: this has changed! Readme has to be updated.*

Import and start in your application:

```js
// e.g. application.js or admin.js
import Skrw from "@swrs/skrw-js"
import "@swrs/skrw-js/dist/index.css"

const skrw = new Skrw()
skrw.start()
```

## Stimulus Controllers

- [Tags](#tags-controller)
- [Markdown Editor](#mde-controller)

<a name="tags-controller"></a>
### Skrw Tags Controller

The `skrw-tags` controller is used on text input fields to provide an interface for comma-seperated tag lists with optional suggestions using [yairEO/tagify](https://github.com/yairEO/tagify). 

Usage:

```html
<!-- simple tags input -->
<input type="text" value="apple, pear, banana" data-controller="skrw-tag">

<!-- tags input fetching suggestions from whitelistUrl -->
<input type="text" value="apple, pear, banana" data-controller="skrw-tag" data-skrw-tags-whitelist-url="/tags/index.json">
```

- `data-skrw-tags-whitelist-url` (string)
  Url for a `application/json` request to fetch whitelist of tags. Expected response format is `[{ value: "apple" }, { value: "banana" }, …]`

- `data-skrw-tags-enforce-whitelist` (boolean, default = false)
  ONLY allow whitelisted tags (requires fetched whitelist from `whitelistUrl`).

<a name="mde-controller"></a>
### Skrw Markdown Editor Controller

The `skrw-mde` controller is used on `textarea` to create a markdown editor using [EasyMDE – Markdown Editor](https://github.com/Ionaru/easy-markdown-editor). 

Usage:

```html
<!-- basic mde editor with full toolbar -->
<textarea data-controller="skrw-mde"></textarea>

<!-- mde editor with customized toolbar -->
<textarea data-controller="skrw-mde" data-skrw-mde-toolbar="link, italic, bold"></textarea>
```

- `data-skrw-mde-toolbar` (comma-seperated list)
  Comma-seperated list to set the available tools in the toolbar, see [EasyMDE](https://github.com/Ionaru/easy-markdown-editor) for all available toolbar options.

## Development

To develop *skrw-js* while running *Rails*'s *Webpacker* it might be helpful to link to the local repository of *skrw-js* and watch for changes in your application's `node_modules`:

```bash
yarn add link:/path/to/skrw-js
```

```js
// config/webpack/development.js
process.env.NODE_ENV = process.env.NODE_ENV || 'development'
const environment = require('./environment')
environment.config.set("devServer.watchOptions.ignored", "/node_modules\/(?!@swrs).*/")

module.exports = environment.toWebpackConfig()
```
