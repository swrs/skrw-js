module.exports = {
  prefix: "skrw-",
  important: true,
  corePlugins: [
    "preflight",
    "alignItems",
    "borderWidth",
    "display",
    "flex",
    "flexDirection",
    "flexGrow",
    "flexShrink",
    "flexWrap",
    "justifyContent",
    "margin",
    "maxHeight",
    "maxWidth",
    "padding",
    "space",
    "textAlign",
    "width",
    "wordBreak"
  ],
  theme: {
    screens: {
      "sm": "376px",
      "md": "1000px",
      "lg": "1440px"
    }
  }
}
