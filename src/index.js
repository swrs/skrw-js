import "./css/index.css"

import SkrwDashboardController from "./js/controllers/skrw_dashboard_controller.js"
import SkrwFlashController from "./js/controllers/skrw_flash_controller.js"
import SkrwDatepickerController from "./js/controllers/skrw_datepicker_controller.js"
import SkrwMdeController from "./js/controllers/skrw_mde_controller.js"
import SkrwSortableController from "./js/controllers/skrw_sortable_controller.js"
// import SkrwTagsController from "./js/controllers/skrw_tags_controller.js"

export default class Skrw {
  
  registerControllers(application) {
    application.register("skrw-dashboard", SkrwDashboardController)
    application.register("skrw-flash", SkrwFlashController)
    application.register("skrw-datepicker", SkrwDatepickerController)
    application.register("skrw-mde", SkrwMdeController)
    application.register("skrw-sortable", SkrwSortableController)
    // application.register("skrw-tags", SkrwTagsController)
  }
}
