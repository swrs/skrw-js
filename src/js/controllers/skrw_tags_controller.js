import { Controller } from "stimulus"
import Tagify from "@yaireo/tagify"
import "@yaireo/tagify/dist/tagify.css"

export default class extends Controller {

  initialize() {
    // initialize tagify after setting options
    this.options().then(options => {
      this.tagify = new Tagify(this.element, options)
      this.tagify
        .on("add", event => { this.formatInput() })
        .on("remove", event => { this.formatInput() })

      this.formatInput()
    })

  }

  async options() {
    // set whitelist
    let whitelist = []
    const url = this.whitelistUrl

    if (url) {
      const response = await fetch(url, {
        headers: {
          "Accept": "application/json"
        }
      })

      const json = await response.json()
      whitelist = json
    }

    // return tagify options object
    return {
      enforceWhitelist: this.enforceWhitelist,
      whitelist: whitelist,
      dropdown: { enabled: 0 }
    }
  }

  formatInput() {
    const value = this.tagify.value.map(t => { return t.value }).join(", ")
    this.element.value = value 
  }

  // data-skrw-whitelist-url = string
  // url for ajax request to fetch whitelist
  get whitelistUrl() {
    return this.data.has("whitelistUrl") ? this.data.get("whitelistUrl") : false
  }

  // data-skrw-tags-enforce-white-list
  // should only allow whitelisted tags (default: false)
  get enforceWhitelist() {
    return this.data.get("enforceWhitelist") == "true" ? true : false
  }
}
