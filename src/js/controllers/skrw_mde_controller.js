import { Controller } from "stimulus"
import easyMDE from "easymde"
import "easymde/dist/easymde.min.css"

export default class extends Controller {

  connect() {
    console.log("connect skrw-mde", this.element)

    let options = {
      element: this.element,
      autoDownloadFontAwesome: undefined,
      forceSync: true,
      minHeight: "var(--skrw-form-mde-min-height)",
      status: false,
      spellChecker: false,
      blockStyles: {
        bold: "**",
        italic: "*"
      },
      toolbar: this.toolbar
    }

    this.easyMDE = new easyMDE(options)
  }

  get toolbar() {
    if (this.data.has("toolbar")) {
      return this.data.get("toolbar").split(",").map(value => { return value.trim() })
    } else {
      return false
    }
  }
}
