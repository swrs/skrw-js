// Date(time) picker controller using flatpickr
//
// Init the controller on an input using <input data-controller="skrw-datepicker">
//
// Available options set by data-attributes:
//
// data-enable-time="true": enable time picker (default: false)
// data-no-calendar="true": disable calendar date picker (default: false)
// data-date-format="Y-m-d H:i": set date input format
// data-alt-format="Y-m-d H:i": set visible ("human readable") format
// data-min-date="2020-10-25": set a min date (default: false)  
// data-max-date="2020-10-26": set a max date (default: false)  
//

import { Controller } from "stimulus"
import flatpickr from "flatpickr"
import "flatpickr/dist/flatpickr.min.css"

export default class extends Controller {

  initialize() {
    this.flatpickr = flatpickr(this.element, this.options)
  }

  get options() {
    return {
      enableTime: this.enableTime,
      noCalendar: this.noCalendar,
      dateFormat: this.datetimeFormat,
      altInput: this.altFormat,
      altFormat: this.altFormat,
      minDate: this.minDate,
      maxDate: this.maxDate,
      time_24hr: true
    }
  }

  // enable time picker
  get enableTime() {
    return this.data.get("enableTime") == "true" ? true : false
  }

  // disable calendar (eg. for time picker)
  get noCalendar() {
    return this.data.get("noCalendar") == "true" ? true : false
  }

  // set datetime format
  get datetimeFormat() {
    const format = "Y-m-d H:i"
    return this.data.has("datetimeFormat") ? this.data.get("datetimeFormat") : format
  }

  // set alternative "human readable" format
  get altFormat() {
    return this.data.has("altFormat") ? this.data.get("altFormat") : false
  }

  // set a min date
  get minDate() {
    return this.data.has("mindate") ? this.data.get("minDate") : false
  }

  // set a max date
  get maxDate() {
    return this.data.has("maxDate") ? this.data.get("maxDate") : false
  }
}
