import { Controller } from "stimulus"

export default class extends Controller {

  connect() {
    this.timeoutId = window.setTimeout(() => {
      this.move(this.element).then(() => {
        this.element.parentNode.removeChild(this.element)
      })
    }, this.delay)
  }

  move(element) {
    this.inTransition = true

    return new Promise(resolve => {
      
      // callback for transitionend event listener
      const callback = () => {
        this.inTransition = false
        resolve()
      }

      element.addEventListener("transitionend", callback)
      element.classList.add("--hidden")
    })

  }

  get delay() {
    return 3 * 1000
  }

  set inTransition(bool) {
    this.data.set("inTransition", bool)
  }

  get inTransition() {
    return this.data.get("inTransition") == "true" ? true : false
  }
}
