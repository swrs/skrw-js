import { Controller } from "stimulus"

export default class extends Controller {

  static targets = ["control"]

  connect() {
    this.expanded = this.expanded

    if (!this.hasControlBtnTarget) {
      const button = document.createElement("div")
      button.setAttribute("data-target", "skrw-dashboard.control")
      button.setAttribute("data-action", "click->skrw-dashboard#toggle")
      this.element.append(button)
    }
  }

  toggle() {
    this.expanded = !this.expanded
  }

  get expanded() {
    return this.data.get("expanded") == "true" ? true : false
  }

  set expanded(bool) {
    this.data.set("expanded", bool)
    const className = "--expanded"
    this.element.classList.toggle(className, bool)
  }
}
