import { Controller } from "stimulus"
import Sortable from "sortablejs"

export default class extends Controller {

  // init sortable controller
  // mandatory data-attributes are url, name, attribute
  //  url = url to send patch request to
  //  name = name of the record to be patched
  //  attribute = attribute of the record to be updated by index
  connect() {
    this.sortable = new Sortable(this.element, {
      animation: 150,
      onEnd: (event) => {
        const url = event.item.getAttribute("data-skrw-sortable-url")
        const index = event.newIndex
        this.submit(url, index)
      }
    }) 

    this.element.classList.add("skrw-list", "--sortable")
  }

  // submit new index to url using name and attribute
  // name = name of updated record
  // attribute = attribute to be updated by index
  // eg. name = "post", attribute = "number" would 
  //     patch /posts/1 with params: { post: { number: index } }
  submit(url, index) {
    const csrfToken = document.querySelector("[name='csrf-token']").content

    // construct payload / params ou of
    // name of the updated record
    // attribute to be updated with new index
    const name = this.data.get("name")
    const attribute = this.data.get("attribute")

    let payload = {}
    payload[name] = {}
    payload[name][attribute] = index
    payload = JSON.stringify(payload)

    fetch(url, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "X-CSRF-Token": csrfToken
      },
      body: payload
    })
  }
}
